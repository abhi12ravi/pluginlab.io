# Plugin website
> Plugin in a FOSS club at PESITBSC

[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]

## Installation


## Usage example


## Development setup


## Release History

* 0.0.1
    * Work in progress

## Meta


Distributed under the XYZ license. See ``LICENSE`` for more information.

[https://gitlab.com/plugin](https://gitlab.com/plugin)
[GitLab repository](https://gitlab.com/plugin/plugin.gitlab.io)

## Contributing

1. Fork it (<https://gitlab.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://gitlab.com/yourname/yourproject/wiki
